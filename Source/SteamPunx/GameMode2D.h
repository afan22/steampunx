// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "APlayerController2D.h"
#include "Character2D.h"
#include "GameFramework/GameMode.h"
#include "GameMode2D.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPUNX_API AGameMode2D : public AGameMode
{
	GENERATED_BODY()

protected:
  virtual void BeginPlay() override;
};
