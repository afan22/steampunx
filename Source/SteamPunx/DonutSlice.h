// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/Generators/EnvQueryGenerator_ProjectedPoints.h"
#include "Runtime/AIModule/Classes/DataProviders/AIDataProvider.h"
#include "DonutSlice.generated.h"


/**
 * 
 */
UCLASS()
class STEAMPUNX_API UDonutSlice : public UEnvQueryGenerator_ProjectedPoints
{
  GENERATED_BODY()

    /** min distance between point and context */
    UPROPERTY(EditDefaultsOnly, Category = Generator)
    FAIDataProviderFloatValue InnerRadius;

  /** max distance between point and context */
  UPROPERTY(EditDefaultsOnly, Category = Generator)
    FAIDataProviderFloatValue OuterRadius;

  /** number of rings to generate */
  UPROPERTY(EditDefaultsOnly, Category = Generator)
    FAIDataProviderIntValue NumberOfRings;

  /** number of items to generate for each ring */
  UPROPERTY(EditDefaultsOnly, Category = Generator)
    FAIDataProviderIntValue PointsPerRing;

    UPROPERTY(EditDefaultsOnly, Category = Generator)
    TSubclassOf<UEnvQueryContext> GenerateAround;

    /** If you generate items on a piece of circle you define angle of Arc cut here */
    UPROPERTY(EditDefaultsOnly, Category = Generator)
      FAIDataProviderFloatValue StartAngleOne;

    UPROPERTY(EditDefaultsOnly, Category = Generator)
      FAIDataProviderFloatValue EndAngleOne;

   

    /** If you generate items on a piece of circle you define angle of Arc cut here */

    UPROPERTY(EditDefaultsOnly, Category = Generator)
      FAIDataProviderFloatValue StartAngleTwo;

    UPROPERTY(EditDefaultsOnly, Category = Generator)
      FAIDataProviderFloatValue EndAngleTwo;

    

    virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;

    virtual FText GetDescriptionTitle() const override;
    virtual FText GetDescriptionDetails() const override;

protected:
  float AngleConversion(float in)const ;
  bool IsAngleAllowed(float TestAngleRad ,float StartOne, float EndOne, float StartTwo, float EndTwo) const;
};
