// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AnimState.h"
#include "Engine/World.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "AICharacter2D.h"
#include "AIController.h"
#include "AIController2D.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPUNX_API AAIController2D : public AAIController
{
	GENERATED_BODY()
private:



private:
  AAIController2D();

  /*Blackboard key*/
  UPROPERTY(EditDefaultsOnly, Category = "AI")
    FName BlackboardKey = "Target";

  // We can set Blackboard in BP
  UPROPERTY(EditDefaultsOnly, Category = "AI")
    UBlackboardData* BlackboardToUse;

  // We can set BehaviorTree in BP
  UPROPERTY(EditDefaultsOnly, Category = "AI")
    UBehaviorTree* BehaviorTreeToUse;

  UPROPERTY()
    UBlackboardComponent* BB;
protected:

  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enum")
    EAnimState AnimationState = EAnimState::EA_idle;

 
 
public:
  /*Define input delegation functions
#######################################*/

// Delegates moving forward/backward intent
  UFUNCTION()
    void MoveForward(float Val);

  // Delegates strafing movement, left and right intent
  UFUNCTION()
    void MoveRight(float Val);

  UFUNCTION()
    void Punch();

  UFUNCTION()
    void Heavy();

 

  /*Sets the sensed target in the blackboard*/
  void SetSeenTarget(APawn* Pawn);
};
