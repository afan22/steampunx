// Fill out your copyright notice in the Description page of Project Settings.

#include "AICharacter2D.h"
#include "AIController2D.h"


// Sets default values
AAICharacter2D::AAICharacter2D()
{
  
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
  	

 


 

  //Initializing the pawn sensing component
  PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
  //Set the peripheral vision angle to 90 degrees
  PawnSensingComp->SetPeripheralVisionAngle(180.f);
  

  if (PawnSensingComp)
  {
   

    FScriptDelegate fScriptDelegate;
    fScriptDelegate.BindUFunction(this, "OnSeePawn");
    PawnSensingComp->OnSeePawn.AddUnique(fScriptDelegate);
  }

 
}

// Called when the game starts or when spawned
void AAICharacter2D::BeginPlay()
{
	Super::BeginPlay();
	
  

  /*if (PawnSensingComp)
  {
    PawnSensingComp->OnSeePawn.AddDynamic(this, &AAICharacter2D::OnSeePawn);
    //DEV
    UE_LOG(LogTemp, Warning, TEXT("init sucsess"));

  }*/
  //DEV

}

void AAICharacter2D::PostInitializeComponents()
{
  Super::PostInitializeComponents();

 
}
  
// Called every frame
void AAICharacter2D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
/*void AAICharacter2D::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}*/

/*void AAICharacter2D::MoveRight(float val)
{
  if (val != 0.0f && !isAnimPlaying)
  {
    GetCharacterMovement()->MaxWalkSpeed = Y_WALK_SPEED;
    AddMovementInput(GetActorRightVector(), (val));

   
  }
  animationState = animStateMachine();
  stateChanged();

}

void AAICharacter2D::MoveForward(float val)
{
  if (val != 0.0f && !isAnimPlaying)
  {


    GetCharacterMovement()->MaxWalkSpeed = X_WALK_SPEED;
    AddMovementInput(GetActorForwardVector(), val);



  }
  animationState = animStateMachine();
  stateChanged();


}

void AAICharacter2D::Punch()
{

  singleStateParamCheck(EAnimState::EA_lightpunch);
  stateChanged();
}

void AAICharacter2D::Heavy()
{
  singleStateParamCheck(EAnimState::EA_heavypunch);
  stateChanged();
}



EAnimState AAICharacter2D::singleStateParamCheck(const EAnimState desiredState)
{
  if (!isAnimPlaying)
  {

    switch (desiredState)
    {
    case EAnimState::EA_lightpunch:
      isAnimPlaying = true;
      animationState = EAnimState::EA_lightpunch;
      break;
    case EAnimState::EA_heavypunch:
      isAnimPlaying = true;
      animationState = EAnimState::EA_heavypunch;
      break;
    }
  }
  return animationState;
}
*/
/*
//TODO replace animStateMachine with a real state machine
EAnimState AAICharacter2D::animStateMachine()
{
  EAnimState returnState = EAnimState::EA_idle;

  yVal = this->GetVelocity().Y;
  xVal = this->GetVelocity().X;

  //Direction Facing
  //================================================
  if (yVal != 0.0f || xVal != 0.0f)
  {
    if (yVal > 0.0f)
    {
      isFacingRight = true;

    }
    else if (yVal < 0.0f)
    {
      isFacingRight = false;

    }
  }

  // If no Animation is playing deside what walking state to set
  //========================================================================
  if (!isAnimPlaying)
  {
    returnState = isFacingRight ? EAnimState::EA_walkright : EAnimState::EA_walkleft;
    if (yVal == 0.0f && xVal == 0.0f)
    {
      returnState = EAnimState::EA_idle;
    }
  }

  //Disregard it if no change occurs or another animation is playing
  //========================================================================
  else
    returnState = EAnimState::EA_previous;

  if (returnState == animationState)
  {
    returnState = EAnimState::EA_previous;
  }
  return returnState;
}
*/
void AAICharacter2D::OnSeePawn(APawn* Pawn)
{
  AAIController2D* AIController = Cast<AAIController2D>(GetController());
 
  if (AIController)
  {
 
    AIController->SetSeenTarget(Pawn);
  }
}

/*void AAICharacter2D::stateChanged_Implementation()
{
}*/