// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AnimState.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PaperFlipbookComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/DamageType.h"
#include "Character2D.generated.h"

class UInputComponent;
class UCameraComponent;
class UPaperFlipbookComponent;
class UBoxComponent;
class UDamageType;

UCLASS()
class STEAMPUNX_API ACharacter2D : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacter2D();

protected:



	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
    float health = 100.0f;

  ///State Machine Variables
  //=================================================================
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CAnimation")
    bool isAnimPlaying = false;


  float xVal = 0.0f;
  float yVal = 0.0f;

  bool isFacingRight = true;

  

public:	
  
  static const int MAX_HIT_COUNT = 10;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
   class UCameraComponent* CameraComponent;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    class UPaperFlipbookComponent* FlipbookComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    class USpringArmComponent* SpringArmComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    class UBoxComponent* AttackBoxComponent;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Movement")
   float X_WALK_SPEED = 400.0f;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
    float Y_WALK_SPEED = 400.0f;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
     float SPRING_ARM_LENGTH=300.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CAnimation")
    EAnimState animationState = EAnimState::EA_idle;
  
  UFUNCTION(BlueprintCallable, Category = "CAnimation")
    EAnimState animStateMachine( );

  UFUNCTION(BlueprintCallable, Category = "CAnimation")
    EAnimState singleStateParamCheck(const EAnimState desiredState);

  UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CEvent")
    void stateChanged();


  UFUNCTION()
    void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

  UFUNCTION()
    float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

  UFUNCTION()
    void MoveRight(float val);

  UFUNCTION()
    void MoveForward(float val);

  UFUNCTION()
    void Punch();

  UFUNCTION()
    void Heavy();
 
  
 
  
 

};
