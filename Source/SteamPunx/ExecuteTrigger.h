// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Engine/TriggerVolume.h"
#include "ExecuteTrigger.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPUNX_API AExecuteTrigger : public ATriggerVolume
{
	GENERATED_BODY()

public:
  AExecuteTrigger();


  // overlap begin function
  UFUNCTION()
    void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

  // overlap end function
  UFUNCTION()
    void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);
protected:
  

};
