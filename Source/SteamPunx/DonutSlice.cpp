// Fill out your copyright notice in the Description page of Project Settings.


#include "DonutSlice.h"

#define LOCTEXT_NAMESPACE "EnvQueryGenerator"

void UDonutSlice::GenerateItems(FEnvQueryInstance& QueryInstance) const
{

  TArray<FVector> CenterPoints;
  QueryInstance.PrepareContext(GenerateAround, CenterPoints);

  if (CenterPoints.Num() <= 0)
  {
    return;
  }

  UObject* BindOwner = QueryInstance.Owner.Get();
  InnerRadius.BindData(BindOwner, QueryInstance.QueryID);
  OuterRadius.BindData(BindOwner, QueryInstance.QueryID);
  NumberOfRings.BindData(BindOwner, QueryInstance.QueryID);
  PointsPerRing.BindData(BindOwner, QueryInstance.QueryID);
  EndAngleOne.BindData(BindOwner, QueryInstance.QueryID);
  StartAngleOne.BindData(BindOwner, QueryInstance.QueryID);
  EndAngleTwo.BindData(BindOwner, QueryInstance.QueryID);
  StartAngleTwo.BindData(BindOwner, QueryInstance.QueryID);

  float ArcRangeOneValue = EndAngleOne.GetValue();
  float ArcRangeTwoValue = EndAngleTwo.GetValue();

  const float StartRangeOne = StartAngleOne.GetValue();
  const float StartRangeTwo = StartAngleTwo.GetValue();

  float InnerRadiusValue = InnerRadius.GetValue();
  float OuterRadiusValue = OuterRadius.GetValue();
  int32 NumRings = NumberOfRings.GetValue();
  int32 NumPoints = PointsPerRing.GetValue();

  if ((InnerRadiusValue < 0.f) || (OuterRadiusValue <= 0.f) ||
    (InnerRadiusValue > OuterRadiusValue) ||
    (NumRings < 1) || (NumPoints < 1))
  {
    return;
  }

  

  const float EndDegOne = FMath::Clamp(ArcRangeOneValue, -360.0f, 360.0f);
  const float EndDegTwo = FMath::Clamp(ArcRangeTwoValue, -360.0f, 360.0f);

  const float StartDegOne = FMath::Clamp(StartRangeOne, -360.0f, 360.0f);
  const float StartDegTwo = FMath::Clamp(StartRangeTwo, -360.0f, 360.0f);

  const float RadiusDelta = (OuterRadiusValue - InnerRadiusValue) / (NumRings - 1);
  const float AngleDelta = 2.0 * PI / NumPoints;
  float SectionAngle = FMath::DegreesToRadians(StartDegOne);
  float SectionAngleTwo = FMath::DegreesToRadians(StartDegTwo);

  TArray<FNavLocation> Points;
  Points.Reserve(NumPoints * NumRings);


  for (int32 SectionIdx = 0; SectionIdx < NumPoints; SectionIdx++, SectionAngle += AngleDelta)
  {
    if (IsAngleAllowed(SectionAngle, StartDegOne, EndDegOne,StartDegTwo,EndDegTwo))
    {

      const float SinValue = FMath::Sin(SectionAngle);
      const float CosValue = FMath::Cos(SectionAngle);

      float RingRadius = InnerRadiusValue;
      for (int32 RingIdx = 0; RingIdx < NumRings; RingIdx++, RingRadius += RadiusDelta)
      {
        const FVector RingPos(RingRadius * CosValue, RingRadius * SinValue, 0.0f);
        for (int32 ContextIdx = 0; ContextIdx < CenterPoints.Num(); ContextIdx++)
        {
          const FNavLocation PointPos = FNavLocation(CenterPoints[ContextIdx] + RingPos);
          Points.Add(PointPos);
        }
      }
    }
  }

  ProjectAndFilterNavPoints(Points, QueryInstance);
  StoreNavPoints(Points, QueryInstance);
}

float UDonutSlice::AngleConversion(float in) const
{
  float angle = in;
  angle = static_cast<int>(angle) % 360 + (angle - FMath::TruncToInt(angle));
  if (!(angle > 0.0f))
    angle += 360.0f;
  return angle;
}

bool UDonutSlice::IsAngleAllowed(float TestAngleRad, float StartOne,float EndOne,float StartTwo,float EndTwo) const
{
  /*const float TestAngleDeg = FMath::RadiansToDegrees(TestAngleRad);
  const float AngleDelta = FRotator::NormalizeAxis(TestAngleDeg - BisectAngleDeg);

  const float TestAngleDegTwo = FMath::RadiansToDegrees(TestAngleRadTwo);
  const float AngleDeltaTwo = FRotator::NormalizeAxis(TestAngleDegTwo - BisectAngleDegTwo);
  return (((FMath::Abs(AngleDelta) - 0.01f) < AngleRangeDeg)&& ((FMath::Abs(AngleDeltaTwo) - 0.01f) < AngleRangeDegTwo));*/

  bool isAllowed = false;
   float TestAngleDeg = AngleConversion(FMath::RadiansToDegrees(TestAngleRad));
   float EndOneN = AngleConversion(EndOne);
   float EndTwoN= AngleConversion(EndTwo);
   float StartOneN = AngleConversion(StartOne);
   float StartTwoN = AngleConversion(StartTwo);

   if (StartOneN < EndOneN)
     isAllowed = StartOneN <= TestAngleDeg && TestAngleDeg <= EndOneN;
   else
     isAllowed = StartOneN <= TestAngleDeg || TestAngleDeg <= EndOneN;

   if (!isAllowed) {
     if (StartTwoN < EndTwoN)
       isAllowed = StartTwoN <= TestAngleDeg && TestAngleDeg <= EndTwoN;
     else
       isAllowed = StartTwoN <= TestAngleDeg || TestAngleDeg <= EndTwoN;
   }

   return isAllowed;
  //return(((TestAngleDeg > StartOne) && (TestAngleDeg < EndOne)) || (((TestAngleDeg > StartTwo) && (TestAngleDeg < EndTwo))));

}

FText UDonutSlice::GetDescriptionTitle() const
{
  FFormatNamedArguments Args;
  Args.Add(TEXT("DescriptionTitle"), Super::GetDescriptionTitle());
  Args.Add(TEXT("DescribeContext"), UEnvQueryTypes::DescribeContext(GenerateAround));

  return FText::Format(LOCTEXT("DescriptionGenerateDonutAroundContext", "{DescriptionTitle}: generate items around {DescribeContext}"), Args);
}

FText UDonutSlice::GetDescriptionDetails() const
{
  FFormatNamedArguments Args;
  Args.Add(TEXT("InnerRadius"), FText::FromString(InnerRadius.ToString()));
  Args.Add(TEXT("OuterRadius"), FText::FromString(OuterRadius.ToString()));
  Args.Add(TEXT("NumRings"), FText::FromString(NumberOfRings.ToString()));
  Args.Add(TEXT("NumPerRing"), FText::FromString(PointsPerRing.ToString()));
  

  FText Desc = FText::Format(LOCTEXT("DonutDescription", "radius: {InnerRadius} to {OuterRadius}\nrings: {NumRings}, points per ring: {NumPerRing}"), Args);

  
    FFormatNamedArguments ArcArgs;
    ArcArgs.Add(TEXT("Description"), Desc);
    ArcArgs.Add(TEXT("AngleValue"), EndAngleOne.DefaultValue);
    ArcArgs.Add(TEXT("ArcDirection"), StartAngleOne.GetValue());
    Desc = FText::Format(LOCTEXT("DescriptionWithArc", "{Description}\nLimit to {AngleValue} angle "), ArcArgs);
  

  FText ProjDesc = ProjectionData.ToText(FEnvTraceData::Brief);
  if (!ProjDesc.IsEmpty())
  {
    FFormatNamedArguments ProjArgs;
    ProjArgs.Add(TEXT("Description"), Desc);
    ProjArgs.Add(TEXT("ProjectionDescription"), ProjDesc);
    Desc = FText::Format(LOCTEXT("OnCircle_DescriptionWithProjection", "{Description}, {ProjectionDescription}"), ProjArgs);
  }

  return Desc;
}


#undef LOCTEXT_NAMESPACE