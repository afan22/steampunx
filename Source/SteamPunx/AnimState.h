// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedEnum.h"
#include "AnimState.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EAnimState : uint8
{
  EA_idle UMETA(DisplayName="Idle"),
  EA_walkright UMETA(DisplayName = "WalkRight"),
  EA_walkleft UMETA(DisplayName = "WalkLeft"),
  EA_lightpunch UMETA(DisplayName = "LightPunch"),
  EA_heavypunch UMETA(DisplayName = "HeavyPunch"),
  EA_previous UMETA(DisplayName="Previous"),
  EA_hit UMETA(DisplayName = "Hit")
};
