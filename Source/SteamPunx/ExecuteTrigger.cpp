// Fill out your copyright notice in the Description page of Project Settings.

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#define printFString(text, fstring) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT(text), fstring)


#include "ExecuteTrigger.h"
AExecuteTrigger::AExecuteTrigger()
{
  //Register Events
  OnActorBeginOverlap.AddDynamic(this, &AExecuteTrigger::OnOverlapBegin);
  OnActorEndOverlap.AddDynamic(this, &AExecuteTrigger::OnOverlapEnd);
}

void AExecuteTrigger::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
  if (OtherActor && (OtherActor != this))
  {
    print("Overlap Begin");
    printFString("Other Actor = %s", *OtherActor->GetName()));
  }
}

void AExecuteTrigger::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
  if (OtherActor && (OtherActor != this))
  {
    print("Overlap Ended");
    printFString("%s has left the Trigger Volume", *OtherActor->GetName()));
  }
}