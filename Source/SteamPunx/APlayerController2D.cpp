// Fill out your copyright notice in the Description page of Project Settings.


#include "APlayerController2D.h"

void AAPlayerController2D::SetupInputComponent()
{
  
  Super::SetupInputComponent();

  check(InputComponent);

  if (InputComponent != NULL)
  {

    //TODO posible set up jump
  ///InputComponent->BindAction("Jump", IE_Pressed, this, &APuzzlePlayerController::Jump);

    InputComponent->BindAction("Punch", IE_Pressed, this, &AAPlayerController2D::Punch);
    InputComponent->BindAction("Heavy", IE_Pressed, this, &AAPlayerController2D::Heavy);

    InputComponent->BindAxis("MoveForward", this, &AAPlayerController2D::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AAPlayerController2D::MoveRight);

    

 
  }
}

void AAPlayerController2D::BeginPlay()
{
  Super::BeginPlay();
  
    SetInputMode(FInputModeGameOnly());
}


// Delegate input to pawn
void AAPlayerController2D::MoveForward(float Val)
{
  ACharacter2D* pawn = Cast<ACharacter2D>(this->GetPawn());
  if (pawn)
  {
  
    pawn->MoveForward( Val);
  }
}

void AAPlayerController2D::MoveRight(float Val)
{
  ACharacter2D* pawn = Cast<ACharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->MoveRight(Val);
  }
}

void AAPlayerController2D::Punch()
{
  ACharacter2D* pawn = Cast<ACharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->Punch();
  }
}

void AAPlayerController2D::Heavy()
{
  ACharacter2D* pawn = Cast<ACharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->Heavy();
  }
}