// Fill out your copyright notice in the Description page of Project Settings.

#include "Character2D.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"


// Sets default values
ACharacter2D::ACharacter2D()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
  SpringArmComponent->SetupAttachment(GetCapsuleComponent());
  SpringArmComponent->TargetArmLength = SPRING_ARM_LENGTH;
  
  SpringArmComponent->bInheritYaw = false;

  CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("2D Camera"));
  CameraComponent->SetupAttachment(SpringArmComponent);
//  CameraComponent->RelativeLocation = FVector(0, 0, BaseEyeHeight); // Position the camera
  CameraComponent->bUsePawnControlRotation = false;


  FlipbookComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipBook"));
  FlipbookComponent->SetupAttachment(GetCapsuleComponent());
  FlipbookComponent->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));

  AttackBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("AttackCollision"));
  AttackBoxComponent->SetupAttachment(FlipbookComponent);
  AttackBoxComponent->SetBoxExtent(FVector(13.0f, 13.0f, 13.0f));
  AttackBoxComponent->SetCollisionProfileName(TEXT("AttackCollision"));
    
 
}

// Called when the game starts or when spawned
void ACharacter2D::BeginPlay()
{
	Super::BeginPlay();
  AttackBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACharacter2D::OnOverlapBegin);
}

// Called every frame
void ACharacter2D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

 
}

void ACharacter2D::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  TArray< AActor* > overlapped;
  OverlappedComp->GetOverlappingActors(overlapped);
  float damageAmount = 0;

  UE_LOG(LogTemp, Warning, TEXT("Overlap"));
  switch (animationState)
  {
    case EAnimState::EA_lightpunch:
      damageAmount = 10.0f;
      break;
    case EAnimState::EA_heavypunch:
      damageAmount = 20.0f;
      break;
    case EAnimState::EA_previous:
      break;
  }
  AController* PlayerController = Cast<AController>(GetController());
  TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
  FDamageEvent DamageEvent(ValidDamageTypeClass);

  for (int i = 0; i < overlapped.Num(); i++)
  {
    if(overlapped[i]!=this)
    overlapped[i]->TakeDamage(damageAmount, DamageEvent, PlayerController, this);
  }

  return;
}

float ACharacter2D::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
  health -= Damage;
  UE_LOG(LogTemp, Warning, TEXT("Health is: %f"),health);
  singleStateParamCheck(EAnimState::EA_hit);
  stateChanged();
  return health;
}


void ACharacter2D::MoveRight(float val)
{
  if (val != 0.0f && !isAnimPlaying)
  {
    GetCharacterMovement()->MaxWalkSpeed = Y_WALK_SPEED;
    AddMovementInput(GetActorRightVector(), (val));

    
  }
  animationState = animStateMachine();
  stateChanged();
  
}

void ACharacter2D::MoveForward(float val)
{
  if (val != 0.0f && !isAnimPlaying)
  {
   
    
    GetCharacterMovement()->MaxWalkSpeed = X_WALK_SPEED;
    AddMovementInput(GetActorForwardVector(), val);

   
    
  }
  animationState = animStateMachine();
  stateChanged();
  

}

void ACharacter2D::Punch()
{
  
  singleStateParamCheck(EAnimState::EA_lightpunch);
  stateChanged();
}

void ACharacter2D::Heavy()
{
  singleStateParamCheck(EAnimState::EA_heavypunch);
  stateChanged();
}



EAnimState ACharacter2D::singleStateParamCheck(const EAnimState desiredState)
{
  if (!isAnimPlaying)
  {

    switch (desiredState)
    {
    case EAnimState::EA_lightpunch:
      isAnimPlaying = true;
      animationState = EAnimState::EA_lightpunch;
      break;
    case EAnimState::EA_heavypunch:
      isAnimPlaying = true;
      animationState = EAnimState::EA_heavypunch;
      break;
    case EAnimState::EA_hit:
      isAnimPlaying = true;
      animationState = EAnimState::EA_hit;
      break;
    }
  }
  return animationState;
}

//TODO replace animStateMachine with a real state machine
EAnimState ACharacter2D::animStateMachine( )
{
  EAnimState returnState = EAnimState::EA_idle;

  yVal = this->GetVelocity().Y;
  xVal = this->GetVelocity().X;

  /*Direction Facing
  ================================================*/
  if (yVal != 0.0f || xVal != 0.0f)
  {
    if (yVal > 0.0f)
    {
      isFacingRight = true;

    }
    else if (yVal < 0.0f)
    {
      isFacingRight = false;

    }
  }

  /* If no Animation is playing deside what walking state to set
  ========================================================================*/
  if (!isAnimPlaying)
  {
    returnState = isFacingRight ? EAnimState::EA_walkright : EAnimState::EA_walkleft;
    if (yVal == 0.0f && xVal == 0.0f)
    {
      returnState = EAnimState::EA_idle;
    }
  }

  /*Disregard it if no change occurs or another animation is playing
  ========================================================================*/
  else
    returnState = EAnimState::EA_previous;
 
  if (returnState == animationState)
  {
    returnState = EAnimState::EA_previous;
  }
  return returnState;
}

void ACharacter2D::stateChanged_Implementation()
{
}



