// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "AnimState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PaperFlipbookComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "Character2D.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "AICharacter2D.generated.h"

class UInputComponent;
class UCameraComponent;
class UPaperFlipbookComponent;
class UBoxComponent;
class UBlackboardComponent;
class UBehaviorTreeComponent;
class UPawnSensingComponent;



UCLASS()
class STEAMPUNX_API AAICharacter2D : public ACharacter2D
{
	GENERATED_BODY()

private:
 

  UFUNCTION(BlueprintCallable)
    void OnSeePawn(APawn* Pawn);
  
public:
	// Sets default values for this character's properties
	AAICharacter2D();

  /*The Component which is used for the "seeing" sense of the AI*/
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    class UPawnSensingComponent* PawnSensingComp;

  

 

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  virtual void PostInitializeComponents() override;

  
 /* UPROPERTY(EditAnywhere, BlueprintReadWrite)
    class UPaperFlipbookComponent* FlipbookComponent;


  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    class UBoxComponent* AttackBoxComponent;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
    float X_WALK_SPEED = 400.0f;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
    float Y_WALK_SPEED = 400.0f;


  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CAnimation")
    EAnimState animationState = EAnimState::EA_idle;


  ///State Machine Variables
  //=================================================================
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CAnimation")
    bool isAnimPlaying = false;

  float xVal = 0.0f;
  float yVal = 0.0f;

  bool isFacingRight = true;

  EAnimState singleStateParamCheck(const EAnimState desiredState);*/



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


  /*UFUNCTION(BlueprintCallable, Category = "CAnimation")
    EAnimState animStateMachine();

  UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CEvent")
    void stateChanged();

  UFUNCTION()
    void MoveRight(float val);

  UFUNCTION()
    void MoveForward(float val);

  UFUNCTION()
    void Punch();

  UFUNCTION()
    void Heavy();*/


  
};
