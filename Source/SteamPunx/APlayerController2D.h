// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character2D.h"
#include "AnimState.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "APlayerController2D.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class STEAMPUNX_API AAPlayerController2D : public APlayerController
{
	GENERATED_BODY()
	
protected:

  virtual void SetupInputComponent() override;
  virtual void BeginPlay() override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enum")
    EAnimState AnimationState=EAnimState::EA_idle;

public:
  /*Define input delegation functions
	#######################################*/
  
    	// Delegates moving forward/backward intent
    UFUNCTION()
    void MoveForward(float Val);
  
    	// Delegates strafing movement, left and right intent
    UFUNCTION()
     void MoveRight(float Val);
  
    UFUNCTION()
      void Punch();

    UFUNCTION()
      void Heavy();

  
};
