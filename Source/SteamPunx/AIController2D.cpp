// Fill out your copyright notice in the Description page of Project Settings.


#include "AIController2D.h"

AAIController2D::AAIController2D()
{
  bAllowStrafe = true;
  
  
}





void AAIController2D::SetSeenTarget(APawn* Pawn)
{
  //Registers the Pawn that the AI has seen in the blackboard
 
  if (ensure(BB))
  {
  

    BB->SetValueAsObject(BlackboardKey, Pawn);
  }
}

void AAIController2D::BeginPlay()
{

  Super::BeginPlay(); // We run Blueprint-beginplay, if we need it for some reason.

  if (!ensure(BlackboardToUse)) { return; } // We ensure that pointer isn't null
  UseBlackboard(BlackboardToUse, BB);
  if (!ensure(BehaviorTreeToUse)) { return; }// We ensure that BehaviorTree isn't null

  // Run the behavior tree
  RunBehaviorTree(BehaviorTreeToUse);

}

// Delegate input to pawn
void AAIController2D::MoveForward(float Val)
{
  AAICharacter2D* pawn = Cast<AAICharacter2D>(this->GetPawn());
  if (pawn)
  {

    pawn->MoveForward(Val);
  }
}

void AAIController2D::MoveRight(float Val)
{
  AAICharacter2D* pawn = Cast<AAICharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->MoveRight(Val);
  }
}

void AAIController2D::Punch()
{
  AAICharacter2D* pawn = Cast<AAICharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->Punch();
  }
}

void AAIController2D::Heavy()
{
  AAICharacter2D* pawn = Cast<AAICharacter2D>(this->GetPawn());
  if (pawn)
  {
    pawn->Heavy();
  }
}



